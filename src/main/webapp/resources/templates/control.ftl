<script type="text/javascript" src="${publicURL}/${staticpath}/js/jquery-3.1.1.min.js"></script>
<style>
.nopadding{padding: 0px}
</style>
<div class="panel panel-default">
  <div class="panel-body">
	<div id="wrapper">
	    <!-- Page Content -->
	    <div id="page-content-wrapper" style= "width: 1280;">
		<div class="nopadding container-fluid">
	         
		<div style="float:left;">
	                token: <input type="text" style="width=400;" id="token"> 
			<button type="button" style="margin-left: 15px; margin-right: 15px;" class="btn btn-primary btn-warning" id="btnHello">Submit</button>
	        </div>
		<div style="float:left;">
		    mBot speed [0; 200]: <input type="text" name="mspeed" id="mspeed" value="85" style="width:50px; margin-right: 15px;">
		</div>
		<div style="float:left;">
                    GUI speed [0; 20]: <input type="text" name="gspeed" id="gspeed" value="1.5" style="width:50px;">
		</div>
		<div style="float:left;">
			<canvas id="control" width="850" height="485"></canvas>			
            	</div>

		</div>
	    <!-- /#page-content-wrapper -->	
	</div>
  <!-- /#wrapper -->
  </div>
</div>

<script>
    $('#btnHello').on('click', function(event) {
        event.preventDefault(); // To prevent following the link (optional)
        console.log('btnHello');
        $.ajax({
        	crossDomain: true,
        	jsonp: false,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(JSON.parse('{"Hello":"Server"}')),
            dataType: 'json',
            url: '${publicURL}/authAction',
            beforeSend : function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + $('#token').val());
            },
            success: function(data, textStatus, jqXHR){
                console.log('btnHello success');
                console.log(data);
		$('#btnHello').attr("disabled","disabled");
		$('#token').attr("disabled","disabled");
		$.getScript("${publicURL}/${staticpath}/js/index.js", function(){
			 console.log("Script loaded but not necessarily executed.");
		});
            },
            error: function(jqXHR, textStatus, errorThrown){
                console.log('btnHello error'+errorThrown);
            }
        });
    });
</script>