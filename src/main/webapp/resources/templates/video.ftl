<script type="text/javascript" src="${publicURL}/${staticpath}/js/jsmpeg.js"></script>
<style>
.nopadding{padding: 0px}
.nomargin{margin: 0px}
</style>

<div id="wrapper">
    <!-- Page Content -->
    <div id="page-content-wrapper">

        <div class="nopadding container-fluid">
            <div style="float:left;background-color:yellow;">
            </div>
            <div style="float:left;background-color:green;">
                <canvas id="videoCanvas1" width="640" height="478">
                    <p>
                        Please use a browser that supports the Canvas Element, like
                        <a href="http://www.google.com/chrome">Chrome</a>,
                        <a href="http://www.mozilla.com/firefox/">Firefox</a>,
                        <a href="http://www.apple.com/safari/">Safari</a> or Internet Explorer 10
                    </p>
                </canvas>
            </div>             
        </div>
	
            <script type="text/javascript">
                $('#projectcontent').css('float','none');
                $('#page-content-wrapper').css('width','1280');

                // Setup the WebSocket connection and start the player
                function absoluteURL(s) {
                    var l = window.location;
                    return ((l.protocol === "https:") ? "wss://" : "ws://") + l.host + l.pathname + s;
                }

                play('ws://131.130.43.176:8010/stream/output/7','videoCanvas1');
                function play(socketURL, canvasName){
	                var canvas = document.getElementById(canvasName);
	                var player = new JSMpeg.Player(socketURL, {canvas:canvas});
	            }

                $(function(){
                    $('#slide-submenu').on('click',function() {
                        $(this).closest('.list-group').fadeOut('slide',function(){
                            $('.mini-submenu').fadeIn();
                        });
                      });
                    $('.mini-submenu').on('click',function(){
                        $(this).next('.list-group').toggle('slide');
                        $('.mini-submenu').hide();
                    })
                })
            </script>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->