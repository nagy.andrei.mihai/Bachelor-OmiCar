package org.omilab.omirob;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;
import org.omilab.omirob.webapp.conf.Freemarker;
import org.omilab.omirob.webapp.microservice.slot.dao.SlotDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import it.sauronsoftware.cron4j.Scheduler;

import java.io.IOException;

public class WebServerMain {
	private final static Logger LOG = LoggerFactory.getLogger(WebServerMain.class);
	
    public static void main(String[] args) {
	
    	Server server = new Server(8080);
    	new Freemarker().init();
         
        WebAppContext context = new WebAppContext();
        
        context.setContextPath("/");

        try {
        	context.setDescriptor("/webapp/WEB-INF/web.xml");
			context.setResourceBase(new ClassPathResource("/webapp").getURI().toString());
		} catch (IOException e) {
			LOG.error("Class not found", e);
		}
       
        context.setParentLoaderPriority(true);

        server.setHandler(context);

        try {
        	server.start();
        	startScheduler();
        	server.join();
        } catch (Exception e) {
        	LOG.error("Could not start Server", e);
        } finally {

        	server.destroy();
        }
    }
    
    private static void startScheduler(){
        Scheduler s = new Scheduler();
        final String pattern="0 0 * * *";
        s.schedule(pattern, () -> SlotDao.clear());
        LOG.info("Scheduling clean task with pattern: \""+ pattern+"\"");
        s.start();
    }

}
