package org.omilab.omirob.webapp.simple.service;

import purejavacomm.NoSuchPortException;
import purejavacomm.PortInUseException;
import purejavacomm.UnsupportedCommOperationException;

import java.io.IOException;

public interface SimpleMgmtService
{
    String randomNumberString();

    String printCommPorts();

    void moveForward(int speed) throws IOException;

    void moveBackwards(int speed) throws IOException;

    void moveLeft(int speed) throws IOException;

    void moveRight(int speed) throws IOException;

    void stop() throws IOException;

    void init() throws NoSuchPortException, PortInUseException, IOException, UnsupportedCommOperationException;

    void terminate();

    String readSensorData() throws IOException;

    void followLine(int speed) throws IOException;

    void followLineBackwards(int speed) throws IOException;

    void turnRight90Degrees(int speed) throws IOException;

    void turnLeft90Degrees(int speed) throws IOException;

    void test() throws IOException;

    void moveDistance(int speed, double distance) throws IOException; // NELI's USE CASE

    void parkYourself(int speed) throws IOException; // NELI's USE CASE

    void abort() throws IOException; // NELI's USE CASE

    void setFreeToPark(int freeToPark) throws IOException;

    int isFreeToPark() throws IOException;

    int getParkedInSpot(); // ANDREI's USE CASE

    void parkYourself2(int speed) throws IOException; // ANDREI's USE CASE

    void parkInSpot(int spot, int speed) throws IOException; // ANDREI's USE CASE
}
