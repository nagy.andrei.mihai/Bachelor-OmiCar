package org.omilab.omirob.webapp.simple.rest;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import org.json.JSONObject;
import org.omilab.omirob.webapp.microservice.conf.Secured;
import org.omilab.omirob.webapp.simple.service.SimpleMgmtService;
import org.omilab.omirob.webapp.simple.service.SimpleMgmtServiceImpl;
import purejavacomm.NoSuchPortException;
import purejavacomm.PortInUseException;
import purejavacomm.UnsupportedCommOperationException;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

//reverse proxy from austria.omilab.org/omirob/car1/ ->
@Singleton
@Path("/")
public class SimpleRest
{
    private final static String SYSTEM_PROPERTIES = "systemProperties";
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle(SYSTEM_PROPERTIES, Locale.getDefault());

    private SimpleMgmtService simpleMgmtService;
    private static final int howManyMillis = 1000;

    public SimpleRest()
    {
        try
        {
            this.simpleMgmtService = new SimpleMgmtServiceImpl();
            this.simpleMgmtService.init();
        } catch (NoSuchPortException | PortInUseException | IOException | UnsupportedCommOperationException e)
        {
            e.printStackTrace();
        }
    }

    @GET
    @Path("random")
    @Produces(MediaType.TEXT_PLAIN)
    public String randomNumberString(final @Context HttpServletRequest servletRequest)
    {
        return this.simpleMgmtService.randomNumberString();
    }

    @GET
    @Path("ports")
    @Produces(MediaType.TEXT_PLAIN)
    public String printCommPorts(final @Context HttpServletRequest servletRequest)
    {
        return this.simpleMgmtService.printCommPorts();
    }

    @GET
    @Path("sensors")
    @Produces(MediaType.TEXT_PLAIN)
    public String readSensorData(final @Context HttpServletRequest servletRequest) throws IOException
    {
        return this.simpleMgmtService.readSensorData();
    }

    @POST
    @Path("authAction")
    @Secured
    public String authAction(String request)
    {
        JSONObject jsonObjectRequest = new JSONObject(request);
        JSONObject jsonObjectAnswer = new JSONObject();

        jsonObjectAnswer.put("Hello", "Client");

        return jsonObjectAnswer.toString();
    }

    @GET
    @Path("simple")
    @Produces(MediaType.TEXT_HTML)
    public String simpleView(final @Context HttpServletRequest servletRequest)
    {
        StringWriter output = new StringWriter();
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);
        try
        {
            cfg.setClassForTemplateLoading(this.getClass(), "/webapp/resources/templates");
            cfg.setDefaultEncoding("UTF-8");
            cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
            Template temp = cfg.getTemplate("simple.ftl");
            Map<String, Object> data = new HashMap<>();
            data.put("publicURL", "http://131.130.43.176:8024");
            data.put("staticpath", "static");
            temp.process(data, output);
        } catch (Exception e)
        {
            e.printStackTrace();
            output.append("<div style=\"color: red;\">Problem with Template Engine</div>");
        }

        return output.toString();
    }

    // NELI's USE CASE FROM WHERE

    @GET
    @Path("driveForward/{speed}")
    @Produces(MediaType.TEXT_PLAIN)
    @Secured
    public String driveForward(@PathParam("speed") String speed, final @Context HttpServletRequest servletRequest)
    {
        try
        {
            this.simpleMgmtService.moveForward(Integer.parseInt(speed));
            return "mBOT drive forward request sent successfully!";
        } catch (IOException e)
        {
            return e.getMessage();
        }
    }

    @GET
    @Path("driveBackward/{speed}")
    @Produces(MediaType.TEXT_PLAIN)
    @Secured
    public String driveBackward(@PathParam("speed") String speed, final @Context HttpServletRequest servletRequest)
    {
        try
        {
            this.simpleMgmtService.moveBackwards(Integer.parseInt(speed));
            return "mBOT drive backward request sent successfully!";
        } catch (IOException e)
        {
            return e.getMessage();
        }
    }

    @GET
    @Path("driveLeft/{speed}")
    @Produces(MediaType.TEXT_PLAIN)
    @Secured
    public String driveLeft(@PathParam("speed") String speed, final @Context HttpServletRequest servletRequest)
    {
        try
        {
            this.simpleMgmtService.moveLeft(Integer.parseInt(speed));
            return "mBOT drive left request sent successfully!";
        } catch (IOException e)
        {
            return e.getMessage();
        }
    }

    @GET
    @Path("driveRight/{speed}")
    @Produces(MediaType.TEXT_PLAIN)
    @Secured
    public String driveRight(@PathParam("speed") String speed, final @Context HttpServletRequest servletRequest)
    {
        try
        {
            this.simpleMgmtService.moveRight(Integer.parseInt(speed));
            return "mBOT drive right request sent successfully!";
        } catch (IOException e)
        {
            return e.getMessage();
        }
    }

    @GET
    @Path("stop")
    @Produces(MediaType.TEXT_PLAIN)
    @Secured
    public String stop(final @Context HttpServletRequest servletRequest)
    {
        try
        {
            this.simpleMgmtService.stop();
            return "mBOT stop request sent successfully!";
        } catch (IOException e)
        {
            return e.getMessage();
        }
    }

    @GET
    @Path("parkYourself/{speed}")
    @Produces(MediaType.TEXT_PLAIN)
    @Secured
    public String parkYourself(@PathParam("speed") String speed, final @Context HttpServletRequest servletRequest)
    {
        try
        {
            this.simpleMgmtService.parkYourself(Integer.parseInt(speed));
            return "mBOT parkYourself request sent successfully!";
        } catch (IOException e)
        {
            return e.getMessage();
        }
    }

    @GET
    @Path("abort")
    @Produces(MediaType.TEXT_PLAIN)
    @Secured
    public String abort(final @Context HttpServletRequest servletRequest)
    {
        try
        {
            this.simpleMgmtService.abort();
            return "mBOT abort request sent successfully!";
        } catch (IOException e)
        {
            return e.getMessage();
        }
    }

    @GET
    @Path("freeToPark")
    @Produces(MediaType.TEXT_PLAIN)
    @Secured
    public String freeToPark(final @Context HttpServletRequest servletRequest)
    {
        try
        {
            return Integer.toString(simpleMgmtService.isFreeToPark());
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return "IOException";
    }

    // NELI's USE CASE UNTIL HERE


    @PUT
    @Path("parkYourself2/{speed}")
    @Produces(MediaType.TEXT_PLAIN)
    @Secured
    public String parkYourself2(@PathParam("speed") String speed, final @Context HttpServletRequest servletRequest) // ANDREI's USE CASE
    {
        try
        {
            this.simpleMgmtService.parkYourself2(Integer.parseInt(speed));
            return "mBOT parkYourself2 request sent successfully!";
        } catch (IOException e)
        {
            return e.getMessage();
        }
    }

    @GET
    @Path("getParkedInSpot")
    @Produces(MediaType.TEXT_PLAIN)
    @Secured
    public String getParkedInSpot(final @Context HttpServletRequest servletRequest) // ANDREI's USE CASE
    {
        return Integer.toString(simpleMgmtService.getParkedInSpot());
    }

    @PUT
    @Path("park/{speed}/{spot}")
    @Secured
    public String parkInSpot(@PathParam("speed") String speed, @PathParam("spot") String spot, final @Context HttpServletRequest servletRequest) // ANDREI's USE CASE (park itself in spot 101)
    {
        try
        {
            this.simpleMgmtService.parkInSpot(Integer.parseInt(spot), Integer.parseInt(speed));
            return "Park in spot request";
        } catch (IOException e)
        {
            return e.getMessage();
        }
    }

    @GET
    @Path("tokenTest")
    @Secured
    public String tokenTest(final @Context HttpServletRequest servletRequest) // ANDREI's USE CASE (test if token is correct)
    {
        return "Success";
    }

}
