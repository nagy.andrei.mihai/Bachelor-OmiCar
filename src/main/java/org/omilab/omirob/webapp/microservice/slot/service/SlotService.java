package org.omilab.omirob.webapp.microservice.slot.service;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;

import org.omilab.omirob.webapp.microservice.slot.dao.SlotDao;
import org.omilab.omirob.webapp.microservice.slot.model.Slot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by martink82cs on 16.09.2016.
 */
@Service
public class SlotService {
	private final static String SYSTEM_MESSAGES = "systemMessages";
	private final static ResourceBundle resourceBundle = ResourceBundle.getBundle(SYSTEM_MESSAGES, Locale.getDefault());
	private final static Logger LOG = LoggerFactory.getLogger(SlotService.class);
	
	public void validateToken(String token) throws Exception {
        // Check if it was issued by the server and if it's not expired
        // Throw an Exception if the token is invalid
        final ZoneId zoneId = ZoneId.of("Europe/Vienna");
        final ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(Instant.now(), zoneId);
        zonedDateTime.getHour();
        HashMap<Integer, Slot> slots = SlotDao.getSlots();
        for(Slot s:slots.values()){
            int which=zonedDateTime.getHour()*2+zonedDateTime.getMinute()/30;
            if(s.which==which&&token.contains(s.secret)){
                LOG.info("VALID: "+ s.getUserName());
                return;
            }
        }
        throw new Exception("UNAUTHORIZED");
	}
}
