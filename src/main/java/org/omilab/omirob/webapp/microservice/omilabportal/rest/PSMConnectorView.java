package org.omilab.omirob.webapp.microservice.omilabportal.rest;

import org.json.JSONObject;
import org.omilab.omirob.webapp.conf.Freemarker;
import org.omilab.omirob.webapp.microservice.omilabportal.model.GenericRequest;
import org.omilab.omirob.webapp.microservice.omilabportal.model.GenericServiceContent;
import org.omilab.omirob.webapp.microservice.slot.dao.SlotDao;
import org.omilab.omirob.webapp.microservice.slot.model.Slot;
import org.omilab.omirob.webapp.microservice.slot.service.SlotService;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayOutputStream;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;

@Path("/view")
public final class PSMConnectorView {
	private static final int NUM_SLOTS = 48;
	
	@Autowired(required = true)
	private SlotService slotService;
	
	public PSMConnectorView() {
	}
	
	//this is a rudimentary CORS save endpoint for ajax calls from portal pages
	@POST
	@Path("{instanceid}/ajax")
	public String processAjax(String request) {
		JSONObject jsonObjectRequest = new JSONObject(request);
		JSONObject jsonObjectAnswer = new JSONObject();
		
		jsonObjectAnswer.put("Hello", "Client");
			
		return jsonObjectAnswer.toString();
	}

	//portal pages
	@POST
	@Path("/{instanceid}/{endpoint}")
	@Produces("application/json")
	@Consumes("application/json")
	public GenericServiceContent processRequest(final GenericRequest request,
												final @PathParam("instanceid") Long instanceid,
												final @PathParam("endpoint") String endpoint,
												final @Context HttpServletRequest servletRequest) {
			
		if("car-control1".equals(endpoint))
			return control(servletRequest, request);
		
		else if("car-auth1".equals(endpoint)) {
			return auth(servletRequest, request);
		}
		else if ("car-stream1".equals(endpoint)) {
			return stream(endpoint);
		}
		
		return new GenericServiceContent("<div style=\"color: red;\">Unknown Endpoint " +endpoint+ "</div>");
	}
	
	private GenericServiceContent control(HttpServletRequest servletRequest, GenericRequest request) {
		final Map<String, String> submenu = new LinkedHashMap<>();
		submenu.put("anchor1", "Neli Petkova");
		submenu.put("anchor2", "Andrei Nagy");
		
		HashMap vals = new HashMap();
		vals.put("userName", request.getUsername());
		vals.put("staticpath", "static");
		vals.put("publicURL", "http://131.130.43.176:8024"); //FIXME: to be replaced with austria.omilab.org/omirob/car1/

		ByteArrayOutputStream bos;
		try {
			bos = Freemarker.process(vals, "control");
		} catch(Exception e){
			e.printStackTrace();
			return new GenericServiceContent("<div style=\"color: red;\">Problem with Template Engine</div>"+e.toString(), submenu);
		}
		
		return new GenericServiceContent(bos.toString(), submenu);
	}
	
	private GenericServiceContent auth(HttpServletRequest servletRequest, GenericRequest request) {
		try{
			HashMap<Integer, Slot> slots = SlotDao.getSlots();
			synchronized (slots) {
				String button = request.getParams().get("action");
				String userName=request.getUsername().trim();
				HashMap vals = new HashMap();
				if (button != null
						&& button.length() > 0
						&& request.getUsername() != null
						&& !request.getUsername().equals("anonymousUser")) {
					vals.put("method","post");
					int slotNumber = Integer.parseInt(button);
					Slot slot = slots.get(slotNumber);
					if (slot == null) {
						slot = new Slot();
						slot.userName = request.getUsername();
						slot.which = slotNumber;
						Random random = new SecureRandom();
						byte[] bytes = new byte[10];
						random.nextBytes(bytes);
						slot.secret = Base64.getEncoder().encodeToString(bytes);
						slots.put(slotNumber, slot);
						SlotDao.save(slots);
					} else if (slot.userName.equals(request.getUsername())) {
						slots.remove(slotNumber);
						SlotDao.save(slots);
					}
				}
                StringBuilder authTokenBuilder = new StringBuilder();
                for(Slot s: slots.values()){
					if(s.userName.equals(userName))
						authTokenBuilder.append(s.secret).append(",");
				}
                String authToken = authTokenBuilder.toString();
                if(authToken.length()>0)
					authToken=authToken.substring(0,authToken.length()-1);


				HashMap s = new LinkedHashMap();
				for (int i = 0; i < NUM_SLOTS; i++) {
					s.put(i, slots.get(i));
				}
				vals.put("slots", s);
				vals.put("userName", request.getUsername());
				vals.put("staticpath", "static");
				vals.put("publicURL", "http://131.130.43.176:8024"); //FIXME: to be replaced with austria.omilab.org/omirob/car1/
				vals.put("authToken", authToken);

				ByteArrayOutputStream bos = Freemarker.process(vals, "auth");
				return new GenericServiceContent(bos.toString());
			}
		} catch(Exception e){
			e.printStackTrace();
			return new GenericServiceContent("<div style=\"color: red;\">Problem with Template Engine</div>"+e.toString());
		}
	}
	
	private GenericServiceContent stream(String endpoint) {
		try {
			HashMap vals = new HashMap();
			ByteArrayOutputStream bos;
			vals.put("publicURL", "http://131.130.43.176:8024"); //FIXME: to be replaced with austria.omilab.org/omirob/car1/
			vals.put("staticpath", "static");
			bos = Freemarker.process(vals, "video");
			
			return new GenericServiceContent(bos.toString());
		} catch(Exception e){
			e.printStackTrace();
			return new GenericServiceContent("<div style=\"color: red;\">Problem with Template Engine</div>"+e.toString());
		}
	}
	

}