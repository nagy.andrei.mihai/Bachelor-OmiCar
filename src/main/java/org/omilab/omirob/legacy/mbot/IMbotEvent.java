package org.omilab.omirob.legacy.mbot;

public interface IMbotEvent {
    void onButton(boolean pressed);
}
