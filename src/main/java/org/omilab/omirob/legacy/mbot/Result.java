package org.omilab.omirob.legacy.mbot;

public class Result<T> {
    public int index;
    public T result;
    public DeviceType deviceType;

}
